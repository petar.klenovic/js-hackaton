const staraDnevna = {
    tepih: 'plavi',
    uređaju: ['televizor', 'telefon', 'router', 'alexa'],
    lampe: {
        zidna: 'led',
        podna: 'halogen',
        stolna: 'smart'
    },
    kauč: 'zeleni',
    stol: 'stakleni',
    slike: ['priroda', 'kuća'],
    sat: {
        zidni: 'digitalni',
        stolni: 'analogni'
    }
}

const novaDnevna = {
    tepih: 'crveni',
    uređaju: ['televizor', 'telefon', 'mobitel', 'smart home'],
    lampe: {
        zidna: 'smart bulb',
    },
    sat: {
        zidni: 'starinski',
    }
}

const dnevna = (staraDnevna, novaDnevna) => {
    const objLoop = (obj) => {
        let newObj = Object.entries(obj);
        return newObj;
    }
    const oldObj = objLoop(staraDnevna);
    const newObj = objLoop(novaDnevna);
    newObj.forEach(el => {
        oldObj.forEach(a => {
            if (a[0] === el[0]) {
                staraDnevna[el[0]] = el[1]
            }
        })
    });
    return staraDnevna;
}

const solution = dnevna(staraDnevna, novaDnevna)

console.log(solution)

//tocno