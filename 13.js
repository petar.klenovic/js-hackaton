let object = {
    šalice: 7,
    čaše: 10,
    tanjuri: 15,
    vaze: 2,
    kemijske: { plava: 'da' },
    kabel: ['internet', 'HDMI', 'displayport', 7]
}

const viksa = object => {
    let obj = object;
    const kljucevi = Object.keys(obj);
    console.log(kljucevi)
    const vrijednosti = Object.values(obj);
    for (let i = 0; i < vrijednosti.length; i++) {
        const el = vrijednosti[i];
        const kljuc = kljucevi[i];
        console.log(kljuc + " " + el)
        if (Object.prototype.toString.call(el) === "[object Array]") {
            obj[kljuc].push('Pa čekaj, ovo je broj uopće!');
        } else if (Object.prototype.toString.call(el) === "[object Object]") {
            obj[kljuc]["Nova godina"] = "2021";
        } else if (el % 2 !== 0) {
            obj[kljuc] = 2 * el;
        } else {
            obj[kljuc] = el - 3;
        }
    }
    return obj;
}

//meni solution ne radi?
//moj solution

Object.keys(object).forEach(key => { 
    if (typeof object[key] === 'number' && object[key] % 2 !== 0) object[key] = object[key] * 2 
    else if (typeof object[key] === 'number' && object[key] % 2 === 0) object[key] = object[key] - 3
    else if (Array.isArray(object[key])) object[key].push('Pa čekaj, ovo je broj uopće!')
    else if (typeof object[key] === 'object') object[key]['Nova godina'] = 2021
})