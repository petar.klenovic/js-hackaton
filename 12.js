const setSort = zatamnjenje => {
    const djeljitelji = [2, 3, 4, 5, 6, 7, 8, 9];
    let map = new Map();
    let prozori = [];
    zatamnjenje.forEach(staklo => {
        djeljitelji.some(n => {
            if (staklo % n === 0) {
                map.set(staklo, n);
                return true;
            }
        })
    });
    let mapSort = new Map([...map.entries()].sort(((a, b) => b[1] - a[1])));
    mapSort.forEach((value,key) => prozori.push(key));
    return prozori;
}

