function magic(string) {
    string = string.split(',');
    const cijena = parseInt(string.shift());

    if (cijena === 0) {
        return 1;
    }
    if (cijena < 0 || (string.length === 1 && string.includes(""))){ 
        return 0;
    }

    const kovanice = string.map((element) => parseInt(element));

    let string1 = [cijena - kovanice[kovanice.length - 1], kovanice]
    let string2 = [cijena, kovanice.slice(0, -1)]

    return magic(string1.join(",")) + magic(string2.join(","))
}