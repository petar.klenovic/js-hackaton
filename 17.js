class Smoothie {
    constructor(voce) {
        this.voce = voce;
    }
    ingredients() {
        let imena = [];
        this.voce.forEach(el => {
            imena.push(el[0])
        });
        return imena;
    }
    getCost() {
        let ukupnaCijena = 0;
        this.voce.forEach(el => {
            ukupnaCijena = el[1] + ukupnaCijena;
        })
        return ukupnaCijena;
    }
    getPrice() {
        let ukupnaCijenaPdv = 0;
        this.voce.forEach(el => {
            ukupnaCijenaPdv = el[1] * 1.25 + ukupnaCijenaPdv;
        })
        return ukupnaCijenaPdv;
    }
    getName() {
        let punoIme = '';
        this.voce.forEach(el => {
            const l = el[0].length;
            punoIme = punoIme + el[0].slice(0,1) + el[0].slice(l-1, l);
        });
        if (this.voce.lenght === 2) {
            punoIme = punoIme + ' smoothie';
            return punoIme;
        }
        punoIme = punoIme + ' fusion';
            return punoIme;
    }
}