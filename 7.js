const perfect = rgb => {
    let rCheck, gCheck, bCheck;
    if (rgb[0] % 3 === 0) {
        rCheck = true;
    }
    if (rgb[1] % 5 === 0) {
        gCheck = true;
    }
    if (rgb[2] % 7 === 0) {
        bCheck = true;
    }
    if (rCheck && gCheck && bCheck) {
        return 'Da';
    }
    return 'Ne';
}