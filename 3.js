const date = new Date();
const sat = date.getHours();

if (sat > 6 && sat < 10) {
    console.log("Dobro jutro");
} else if (sat > 10 && sat < 18) {
    console.log("Dobar dan");
} else if (sat > 18 && sat < 22) {
    console.log("Hej, već je kasno, bolje bi bilo na popiješ čaj");
} else {
    console.log("Pusti me da spavam");
}

//Točno,s time da se mislilo da uključuje 10 sati u prvom uvjetu, 18 sati u 2. i 22 u 3.uvjetu, ali nema veze, nije dovoljno precizno napisano bilo. :)
