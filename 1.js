const konstrukcija = 943917;
const kroviste = 153.462; 
const stolarija = 132.795; 
const fasada = 84.825; 
const instalacije = 105.595; 
const zavrsniRadovi= 182.315; 
const opremanje = 96.092; 

const ukupnaCijena = konstrukcija + kroviste + stolarija + fasada + instalacije + zavrsniRadovi + opremanje;
console.log(ukupnaCijena);

//Postupak je točan, ali JS je ovu točku shvatio kao decimalnu točku pa konačan rezultat nije točan. U zadatku je točka zapravo bila stavljena tako da označava tisućice, malo je to nespretno jer je nekad točka oznaka za decimalnu točku, a nekad je to zarez, tako da na to treba paziti, pogotovo u programiranju.
